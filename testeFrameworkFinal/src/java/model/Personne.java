/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.MyAnnotation;
import java.util.HashMap;
import utilities.ModelView;

/**
 *
 * @author JOHARY
 */
public class Personne {
    private String id;
    private String nom;
    private String prenom;
    private int salaire;
    
    public Personne(){
        
    }
    
    public Personne(String id,String n,String p,int salaire){
        this.id = id;
        this.nom = n;
        this.prenom = p;
        this.salaire = salaire;
    }

    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    @MyAnnotation(url = "getListPersonne.do")
    public ModelView getPersonnes(){
        ModelView ans = new ModelView();
        HashMap<String, Object> data = new HashMap<String, Object>();
        
        String jsp = "listePersonne.jsp";
        ans.setUrl(jsp);
        
        Personne[] pers = new Personne[5];
        pers[0] = new Personne("id1","Randria","Koto",1400);
        pers[1] = new Personne("id2","Bekoto", "Solo",11000);
        pers[2] = new Personne("id3","Rasoa","Berthe",12000);
        pers[3] = new Personne("id4","Bekoto", "Ben",1300);
        pers[4] = new Personne("id5","Kotoson", "Njila",2000);
        
        data.put("liste", pers);
        ans.setData(data);     
        return ans;
    }
}
