package model;

import annotation.MyAnnotation;
import java.util.HashMap;
import utilities.ModelView;

public class Chien {
	private int age;
	private String nom;
        private Personne personne;

        public Personne getPersonne() {
            return personne;
        }

        public void setPersonne(Personne personne) {
            this.personne = personne;
        }
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
        
    @MyAnnotation(url = "insertAnimal.do")
    public ModelView insertAnimals(){
        ModelView ans = new ModelView();
        HashMap<String, Object> data = new HashMap<String, Object>();
        
        String jsp = "insertChien.jsp";
        ans.setUrl(jsp);
        
        Chien[] animal = new Chien[2];
        animal[0] = new Chien();
        animal[0].age = 12;
        animal[0].personne = new Personne();
        animal[0].getPersonne().setNom("nomPersonne");
        animal[0].nom = "pupuce";
        
        animal[1] = new Chien();
        animal[1].age = 15;
        animal[1].personne = new Personne();
        animal[1].getPersonne().setNom("nomPersonneNumero1");
        animal[1].nom = "nomChien";
        
        data.put("listeAnimal", animal);
        
        Personne[] pers = new Personne[5];
        pers[0] = new Personne("id1","Randria","Koto",1500);
        pers[1] = new Personne("id2","Bekoto", "Solo",2000);
        pers[2] = new Personne("id3","Rasoa","Berthe",300);
        pers[3] = new Personne("id4","Bekoto", "Ben",250);
        pers[4] = new Personne("id5","Kotoson", "Njila",1600);
        
        data.put("personne", pers);
        
        ans.setData(data);     
        return ans;
    }
    
    @MyAnnotation(url = "save.do")
    public ModelView save(Chien unknown){
        HashMap<String,Object> data = new HashMap<String,Object>();
        data.put("animal", unknown);
        
        ModelView ans = new ModelView();
        ans.setData(data);
        String jsp = "ajout.jsp";
        ans.setUrl(jsp);
        
        return ans;
    }
	
}
