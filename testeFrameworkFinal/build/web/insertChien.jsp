<%-- 
    Document   : insertChien
    Created on : 18 nov. 2022, 14:25:35
    Author     : JOHARY
--%>
<%@page import="model.*"%>
<% 
    Chien[] a = (Chien[])request.getAttribute("listeAnimal");
    Personne[] p = (Personne[])request.getAttribute("personne");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste Chien existant</h1>
        <ul>
            <%
                for(Chien x : a){
            %>
                <li>nom = <%=x.getNom() %> , proprietaire = <%=x.getPersonne().getNom() %></li>
            <% } %>
        </ul>
        <form action="save.do" method="POST">
            age : <input type="number" name="age"><br/>
            nom : <input type="text" name="nom"><br/>
            proprietaire : 
            <select name="personne">
                <%
                    for(Personne x : p){
                %>
                <option value="<%=x.getId()%>"><%=x.getPrenom()%></option>
                <%
                    }
                %>
            </select><br/>
            <input type="submit" value="valider">
        </form>
    </body>
</html>
